package com.example.servlets;


import org.apache.catalina.Context;
import org.apache.catalina.Wrapper;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.servlets.DataUtils.putServletIdentificator;

//import org.json.JSONObject;

/**
 * Created by sergey on 12.01.17.
 */
public class MyContainerServlet extends HttpServlet {

    private Context rootContext;

    public void setContext(Context ctx) {
        rootContext = ctx;
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        addServlet("/servlet1","servlet1","servlet1");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String res = req.getReader().readLine();
        JSONObject jsonObject = new JSONObject(res);

        String url = (String) jsonObject.get("url");
        String content = (String) jsonObject.get("content");
        addServlet(url, url, content);
    }

    private void addServlet(String url, String nameIdentity, String content) {
        Wrapper newWrapper = rootContext.createWrapper();
        newWrapper.setName(nameIdentity);
        newWrapper.setLoadOnStartup(1);
        newWrapper.setServletClass(EmbeddedServlet.class.getName());

        rootContext.addChild(newWrapper);
        rootContext.addServletMapping(url, nameIdentity);
        putServletIdentificator(nameIdentity, content);

    }
}

