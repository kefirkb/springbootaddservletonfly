package com.example.configs;

import com.example.servlets.MyContainerServlet;
import org.apache.catalina.*;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.core.StandardEngine;
import org.apache.catalina.core.StandardHost;
import org.apache.catalina.core.StandardService;
import org.apache.catalina.startup.Tomcat;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by kefirkb on 13.01.2017.
 */

//@EnableWebMvc
@Configuration
@ComponentScan
public class AutoConfig  {

    @Bean
    public TomcatEmbeddedServletContainerFactory tomcatFactory() {
        return new TomcatEmbeddedServletContainerFactory() {

            @Override
            protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(
                    Tomcat tomcat) {

                Context context = new StandardContext();
                context.addLifecycleListener(new Tomcat.FixContextListener());
                context.setName("custom-context");
                context.setPath("/");

                MyContainerServlet servlet = new MyContainerServlet();
                servlet.setContext(context);

                Wrapper wrapper = context.createWrapper();
                wrapper.setServlet(servlet);
                wrapper.setName("custom-root-wrapper");

                context.addChild(wrapper);
                //I need to put my root servlet in some main url because if it eq "/" , doGet Method works twice;
                context.addServletMapping("", wrapper.getName());


                tomcat.getHost().addChild(context);

                return super.getTomcatEmbeddedServletContainer(tomcat);
            }
        };
    }
}
